
//problem no.1
function fizzBuzz(num){
   if(num % 3 == 0 && num % 5 == 0)
   {
     return "FizzBuzz";
   }
   else if(num % 3 == 0)
   {
     return "Fizz";
   }
   else if(num % 5 == 0)
   {
      return "Buzz";
   }
   else
   {
    return "Pop";
   }
}

console.log(fizzBuzz(5));
console.log(fizzBuzz(30));
console.log(fizzBuzz(27));
console.log(fizzBuzz(17));


//problem no.2




function roygbiv(letter){

   let convert = letter.toLowerCase();
   
  if(convert == "r")
  {
    return "Red";
  }
  else if(convert == "o")
  {
     return "Orange";
  }
  else if(convert == "y")
  {
    return "Yellow";
  }
  else if(convert == "g")
  {
    return "Green";
  }
  else if(convert == "b")
  {
    return "blue";
  }
  else if(convert == "i")
  {
    return "indigo";
  }
  else if(convert == "v")
  {
    return "violet";
  }
  else 
  {
    return "no color";
  }
}

console.log(roygbiv("r"));
console.log(roygbiv("G"));
console.log(roygbiv("x"));
console.log(roygbiv("B"));
console.log(roygbiv("i"));






function stretchGoal(year){
  if(year % 400 == 0)
  {
    return "Leap Year"
  }
  else if(year % 100 == 0)
  
  {
   return "Is not a Leap Year";
  }
  else if (year % 4 == 0)
  {
    return "Leap Year";
  }
  else
  {
   return "Is not a Leap Year"
  }

}

console.log(stretchGoal(1900));
console.log(stretchGoal(2000));
console.log(stretchGoal(2004));
console.log(stretchGoal(2021));
